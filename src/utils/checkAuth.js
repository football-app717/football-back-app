import jwt from "jsonwebtoken";



export default (req, res, next) => {
    const authHeader = req.headers.authorization || '';
    const token = authHeader.replace(/bearer\s+/i, '');

    if (token) {
        try {
            const decode = jwt.verify(token, '717565boy');
            req.userId = decode._id;
            next();
        } catch (error) {
            if (error instanceof jwt.TokenExpiredError) {
                return res.status(401).json({
                    message: "JWT token has expired"
                });
            } else {
                console.log("Problem here")
                return res.status(403).json({
                    message: "JWT token is not authorized"
                });
            }
        }
    } else {
        return res.status(403).json({
            message: "JWT token is not authorized"
        });
    }
}