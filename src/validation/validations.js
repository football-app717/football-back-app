import {body} from "express-validator"


export const registerValidation = [
    body('username', 'Error in username')
        .isString()
        .withMessage("Username should be string")
        .notEmpty()
        .withMessage("Username can not be empty")
        .isLength(({min: 3}))
        .withMessage("Username is too short"),

    body('email', "Error in email")
        .isEmail()
        .notEmpty()
        .withMessage("Email can not be empty"),
    body('password', 'Error in password')
        .isString()
        .withMessage('Password should be string')
        .notEmpty()
        .withMessage("Password can not be empty")
];
export const loginValidation = [
    body('email', 'Error in email')
        .notEmpty()
        .withMessage("Email can not be empty")
        .isEmail(),
    body('password', 'Error in password')
        .isString()
        .withMessage("Password should be string")
        .isLength({min: 5})
        .withMessage("Error in password")
];
