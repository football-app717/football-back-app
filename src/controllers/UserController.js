import {validationResult} from "express-validator";
import UserModel from "../models/User.js";
import bcrypt, {genSalt} from "bcrypt";
import jwt from "jsonwebtoken";


export const login = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json(errors.array())
        }

        const user = await UserModel.findOne({email: req.body.email});

        if (!user) {
            return res
                .status(404)
                .json({
                    message: 'You should create an account first'
                })
        }

        const isValidPassword = await bcrypt.compare(req.body.password, user._doc.passwordHash);

        if (!isValidPassword) {
            return res
                .status(400)
                .json({
                    message: 'Your email or password is incorrect'
                })
        }

        const token = await jwt.sign({
            _id: user._id
        }, '717565boy', {expiresIn: '30d'});

        const {passwordHash, roles, ...userData} = user._doc;

        return res
            .status(200)
            .json({
                ...userData,
                token
            })
    } catch (err) {
        return res
            .status(500)
            .json({
                message: "Internal error in Auth",
                error: err
            })
    }
};
export const register = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json(errors.array())
        }

        const password = req.body.password;
        const salt = await genSalt(10);
        const hash = await bcrypt.hash(password, salt);

        const doc = new UserModel({
            username: req.body.username,
            email: req.body.email,
            passwordHash: hash
        });

        const user = await doc.save();

        const token = await jwt.sign({
            _id: user._id
        }, '717565boy', {expiresIn: '30d'});

        const {passwordHash, roles, ...userData} = user._doc;

        res
            .status(201)
            .json({
                ...userData,
                token
            });
    } catch (err) {
        if (err.code === 11000 && err.keyPattern && err.keyPattern.username === 1) {
            // Return a unique email error response
            return res.status(400).json({message: "Username is already taken"});
        }

        if (err.code === 11000 && err.keyPattern && err.keyPattern.email === 1) {
            // Return a unique email error response
            return res.status(400).json({message: "Email is already taken"});
        }

        return res
            .status(500)
            .json({
                message: "Internal error in Register",
                error: err
            })
    }
};
export const getAboutMe = async (req, res) => {
    try {
        const user = await UserModel.findById(req.userId);
        if(!user) {
            return res
                .status(404)
                .json({
                    message: "User is not found"
                });
        }

        const {passwordHash, roles, ...userData} = user._doc;

        return res
            .status(201)
            .json({
                ...userData
            })



    } catch (err) {
        return res
            .status(500)
            .json({
                message: "Internal error in get about me",
                error: err
            })
    }
};