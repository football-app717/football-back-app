import mongoose from "mongoose";



const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    roles: {
        type: Array,
        default: ['ROLE_USER']
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    passwordHash: {
        type: String,
        required: true
    }
}, {timestamps: true});



export default mongoose.model('User', UserSchema);
