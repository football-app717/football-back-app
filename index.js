import express from "express";
import {loginValidation, registerValidation} from "./src/validation/validations.js";
import mongoose from "mongoose";
import {getAboutMe, login, register} from "./src/controllers/UserController.js";
import checkAuth from "./src/utils/checkAuth.js";





mongoose
    .connect('mongodb+srv://dadajonovmax:717565boy@cluster0.b5lgurd.mongodb.net/sport?retryWrites=true&w=majority')
    .then((res) => {
        console.log("DB is ok");
    })
    .catch((err) => {
        console.log("Error in db: ", err);
    })

const app = express();

app.use(express.json());

// User start
app.post('/users', registerValidation, register)
app.post("/users/auth", loginValidation, login);
app.get("/users/about_me", checkAuth, getAboutMe);
// User end






app.listen(3000, (err) => {
    if (err) {
        console.log("Server error");
    }
    console.log("Server ok")
})